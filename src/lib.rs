// Copyright 2019 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![allow(clippy::tabs_in_doc_comments)]

//! Allows creating owning iterators out of arrays. This is useful for
//! fixed-size iterators. Try writing the following function:
//!
//! ```compile_fail
//! fn pair_iter(a: i32, b: i32) -> impl Iterator<Item=i32> {
//! 	unimplemented!()
//! }
//! ```
//!
//! This is hard to do because most iterators don't own the values they are iterating over. One ugly solution is:
//!
//! ```
//! fn pair_iter(a: i32, b: i32) -> impl Iterator<Item=i32> {
//! 	Some(a).into_iter().chain(Some(b))
//! }
//!
//! assert_eq!(pair_iter(1, 2).collect::<Vec<_>>(), &[1, 2]);
//! ```
//!
//! This crate allows turning arrays into owning iterators, perfect for short, fixed-size iterators.
//!
//! ```
//! use array_iterator::ArrayIterator;
//!
//! fn pair_iter(a: i32, b: i32) -> impl Iterator<Item=i32> {
//! 	ArrayIterator::new([a, b])
//! }
//!
//! assert_eq!(pair_iter(1, 2).collect::<Vec<_>>(), &[1, 2]);
//! ```

use std::fmt::{self, Debug, Formatter};
use std::mem::MaybeUninit;
use std::ops::Range;

use self::sealed::Array;

#[cfg(test)]
mod tests;

/// An owned iterator over an array.
pub struct ArrayIterator<A: Array> {
	array: A::PartialInit,
	alive: Range<usize>,
}

impl<A: Array> ArrayIterator<A> {
	/// Construct a new owned iterator.
	#[must_use]
	pub fn new(array: A) -> Self {
		let partial = array.into_partial_init();
		let alive = 0..partial.as_ref().len();
		Self {
			array: partial,
			alive,
		}
	}

	fn remaining_slice(&self) -> &[A::Element] {
		let initialized_part = &self.array.as_ref()[self.alive.clone()];
		// SAFETY: self.current is always within bounds
		unsafe {
			&*(initialized_part as *const [MaybeUninit<A::Element>]
				as *const [<A as Array>::Element])
		}
	}
}

impl<A: Array> Iterator for ArrayIterator<A> {
	type Item = A::Element;

	fn next(&mut self) -> Option<Self::Item> {
		let data = self.array.as_mut();
		let i = self.alive.next()?;

		// SAFETY: alive always indexes initialized data
		Some(unsafe { data.get_unchecked_mut(i).as_mut_ptr().read() })
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		(self.len(), Some(self.len()))
	}

	fn count(self) -> usize {
		self.len()
	}
}

impl<A: Array> DoubleEndedIterator for ArrayIterator<A> {
	fn next_back(&mut self) -> Option<Self::Item> {
		let data = self.array.as_mut();
		let i = self.alive.next_back()?;

		// SAFETY: alive always indexes initialized data
		Some(unsafe { data.get_unchecked_mut(i).as_mut_ptr().read() })
	}
}

impl<A: Array> ExactSizeIterator for ArrayIterator<A> {
	fn len(&self) -> usize {
		self.alive.end - self.alive.start
	}
}

impl<A: Array> std::iter::FusedIterator for ArrayIterator<A> {}

impl<A: Array> Clone for ArrayIterator<A>
where
	A::Element: Clone,
{
	fn clone(&self) -> Self {
		let mut new = Self {
			// SAFETY: as PartialInit can be fully uninitialized this is safe
			array: unsafe {
				#[allow(clippy::uninit_assumed_init)]
				<MaybeUninit<A::PartialInit>>::uninit().assume_init()
			},
			alive: 0..0,
		};

		for (src, dst) in self.remaining_slice().iter().zip(new.array.as_mut()) {
			// If cloning panics the current items will correctly be dropped
			*dst = MaybeUninit::new(src.clone());
			new.alive.end += 1;
		}

		new
	}
}

impl<A: Array> Debug for ArrayIterator<A>
where
	A::Element: Debug,
{
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.debug_tuple("ArrayIterator")
			.field(&self.remaining_slice())
			.finish()
	}
}

// https://github.com/rust-lang/rust/issues/37572
// unsafe impl<T, A: Array<T>> std::iter::TrustedLen for ArrayIterator<T, A> { }

impl<A: Array> Drop for ArrayIterator<A> {
	fn drop(&mut self) {
		// We don't actually drop the array itself, just everything inside of it.
		// Hopefully this is fine.
		for _ in self {}
	}
}

// TODO: Use FixedSizeArray https://github.com/rust-lang/rust/issues/27778

macro_rules! impl_array_for_arrays {
    ($($len:literal,)*) => {
        $(
            impl<T> Array for [T; $len] {
                type Element = T;
                type PartialInit = [MaybeUninit<Self::Element>; $len];

                fn into_partial_init(self) -> Self::PartialInit {
                    // We can't use transmute here because Rustc thinks that [T; N] and
                    // [MaybeUninit<T>; N] are different sizes for some reason.

                    // SAFETY: The entire array is initialized and is the same length, and we
                    // forget self right after
                    let partial = unsafe { std::mem::transmute_copy(&self) };
                    std::mem::forget(self);
                    partial
                }
            }
        )*
    }
}

impl_array_for_arrays! {
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
	26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
	50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64,
}

mod sealed {
	use std::mem::MaybeUninit;

	pub trait Array {
		type Element;
		type PartialInit: AsRef<[MaybeUninit<Self::Element>]> + AsMut<[MaybeUninit<Self::Element>]>;

		fn into_partial_init(self) -> Self::PartialInit;
	}
}
